import React from 'react'
import { Link } from 'react-router-dom'

const Results = ({ data }) => (
  <div className="container-home">
    { data.map(item => (
      <div className="container-classes" key={item.id}>
        <h2 className="titleResults">{`${item.entitled}`}</h2>
        <p>{`${item.author}`}</p>
        <p>{`${item.description}`}</p>
        <Link className="btn btn-default btnClasses" to={`/details/${item.id}`}>Suivre le cours</Link>
      </div>
    ))}
  </div>
)

export default Results
