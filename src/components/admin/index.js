import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import classnames from 'classnames'
/* eslint-disable */

class Admin extends Component {
  constructor() {
    super()
    this.state = {
      entitled: '',
      author: '',
      description: '',
      mode: 'javascript',
      theme: 'monokai',
      errors: {}
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()
    const apiUrl = 'http://localhost:3000/class/create'
    axios.post(apiUrl, {
        entitled: this.state.entitled,
        author: this.state.author,
        description: this.state.description,
        mode: this.state.mode,
        theme: this.state.theme
      }).then(function (response) {
        console.log(response.data)
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleInputChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {
    const { errors } = this.state
    return (
      <div className="container adminContainer">
        <h2 style={{ marginBottom: '40px' }}>Admin</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <input
              type="text"
              placeholder="intitulé"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.entitled
              })}
              name="entitled"
              onChange={this.handleInputChange}
              value={this.state.entitled}
            />
            {errors.entitled && (<div className="invalid-feedback">{errors.entitled}</div>)}
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="author"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.author
              })}
              name="author"
              onChange={this.handleInputChange}
              value={this.state.author}
            />
            {errors.author && (<div className="invalid-feedback">{errors.author}</div>)}
          </div>
          <div className="form-group">
            <textarea
              type="text"
              placeholder="description"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.description
              })}
              name="description"
              onChange={this.handleInputChange}
              value={this.state.description}
            />
            {errors.description && (<div className="invalid-feedback">{errors.description}</div>)}
          </div>
          <div className="form-group">
            <select name="mode" value={this.state.mode} onChange={this.handleInputChange} className={classnames('form-control form-control-lg', {
                'is-invalid': errors.mode })}>
              <option value="javascript">javascript</option>
              <option value="html">html</option>
              <option value="css">css</option>
              <option value="java">java</option>
            </select>
            {errors.mode && (<div className="invalid-feedback">{errors.mode}</div>)}
          </div>
          <div className="form-group">
          <select name="theme" value={this.state.theme} onChange={this.handleInputChange} className={classnames('form-control form-control-lg', {
              'is-invalid': errors.theme })}>
            <option value="monokai">monokai</option>
            <option value="html">github</option>
            <option value="tomorrow">tomorrow</option>
            <option value="kuroir">kuroir</option>
          </select>
            {errors.theme && (<div className="invalid-feedback">{errors.theme}</div>)}
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Creer Un cours
            </button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
})

export default connect(mapStateToProps, {})( Admin)
