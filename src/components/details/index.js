import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getDetailsData } from './detailsActions'
import TheDetails from './detailsComponents/theDetails/index.js'

class Details extends Component {
  constructor(props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
    const { match } = this.props
    const idUrl = match.params.id
    getDetailsData(idUrl)
  }

  render() {
    const { details } = this.props
    return (
      <div>
        <TheDetails data={details.data} />
      </div>
    )
  }
}

export default connect(state => state)(Details)
