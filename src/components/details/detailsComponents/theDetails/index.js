import React from 'react'
import { Link } from 'react-router-dom'

/* eslint-disable */
const TheDetails = ({ data }) => (
  <div className="detailsContenair">
    <h2 className="titleDetails">{`${data.entitled}`}</h2>
    <p className="nav-link posts descriptionDetails">{`${data.description}`}</p>
    <Link className="btn btn-default buttonDetails" to={`/editor/${data._id}`}>Voir le cours</Link>
  </div>
)
/* eslint-enable */
export default TheDetails
