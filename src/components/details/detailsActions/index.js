import axios from 'axios'

import actionsType from './actions-type'
import store from '../../../store'

/**
 * Format events
 * @param {Array} events
 * @return {Array} eventsFormatted
 */

const getTheDetails = lessons => ({
  type: actionsType.GET_DETAILS,
  data: lessons
})

export const getDetailsData = (idUrl) => {
  const apiUrl = `http://localhost:3000/class/get/details/${idUrl}`
  axios.get(apiUrl).then((response) => {
    store.dispatch(getTheDetails(response.data))
  }).catch((error) => {
    console.log(error)
  })
}
