import axios from 'axios'
import jwtDecode from 'jwt-decode'
import actionsType from '../actions-types'
import store from '../../../../store.js'

export const currentUser = decoded => ({
  type: actionsType.CURRENT_USER,
  payload: decoded
})

export const userDisconnect = decoded => ({
  type: actionsType.USER_DISCONNECT,
  payload: decoded
})

export const setAuthToken = (token) => {
  if (token) {
    axios.defaults.headers.common.Authorization = token
  } else {
    delete axios.defaults.headers.common.Authorization
  }
}

export const login = (user) => {
  axios.post('http://localhost:3000/auth/login', user)
    .then((res) => {
      const { token } = res.data
      localStorage.setItem('jwtToken', token)
      setAuthToken(token)
      const decoded = jwtDecode(token)
      store.dispatch(currentUser(decoded))
    })
    .catch((err) => {
      store.dispatch({
        type: actionsType.GET_ERRORS,
        payload: err.response.data
      })
    })
}

export const logout = () => {
  localStorage.removeItem('jwtToken')
  store.dispatch(userDisconnect({
    type: actionsType.USER_DISCONNECT
  }))
}
