import axios from 'axios'
import actionsType from '../actions-types'
import store from '../../../../store.js'

export const register = (user, history) => () => {
  axios.post('http://localhost:3000/auth/register', user)
    .then(() => {
      history.push('/login')
    })
    .catch((err) => {
      store.dispatch({
        type: actionsType.GET_ERRORS,
        payload: err.response.data
      })
    })
}
