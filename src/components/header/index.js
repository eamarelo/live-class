import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../user/actions/actionsLogin'

/*eslint-disable*/
class Header extends Component {

  constructor(props) {
    super(props)
    this.state = {
      redirect: false
    }
  }

  componentWillMount() {
    const { auth: { isAuthenticated } } = this.props
    if (!isAuthenticated) {
      this.setState({ redirect: true })
      actions.logout()
    }
  }

  renderRedirect() {
    console.log(this.state.redirect)
    if (this.state.redirect) {
      return <Redirect to='/' />
    }
  }

  setRedirect () {
    this.setState({ redirect: true })
    actions.logout()
  }

  render() {
    const { isAuthenticated, isAdmin, user } = this.props.auth
    const authLinks = (
      <li className="navbar-nav ml-auto">
        <a href="" className="nav-link" onClick={() => this.setRedirect()}>
          Log Out
        </a>
      </li>
    )
    const guestLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/">Se Connecter</Link>
        </li>
      </ul>
    )

    return (
      <nav className="navbar navbar-expand-lg navbar bg-dark navbar-fixed-top header">
        {this.renderRedirect()}
        <Link className="navbar-brand" to="/">
          Live Class
        </Link>
        <div className="collapse navbar-collapse test" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto"> 
            <li className="nav-item">
              <div className="collapse navbar-collapse right" id="navbarSupportedContent">
                {isAuthenticated ? authLinks : guestLinks}
              </div>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

export default connect(mapStateToProps, actions)(Header)

