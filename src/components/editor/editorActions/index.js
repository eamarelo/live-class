import axios from 'axios'

import actionsType from './actions-type'
import store from '../../../store'

/**
 * Format events
 * @param {Array} events
 * @return {Array} eventsFormatted
 */

const getTheEditor = editor => ({
  type: actionsType.GET_EDITOR,
  data: editor
})

export const getEditorData = (idUrl) => {
  const apiUrl = `http://localhost:3000/class/get/details/${idUrl}`

  axios.get(apiUrl).then((response) => {
    store.dispatch(getTheEditor(response.data))
  }).catch((error) => {
    console.log(error)
  })
}
