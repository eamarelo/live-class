import React from 'react'
import AceEditor from 'react-ace'
import io from 'socket.io-client'

import 'brace/mode/javascript'
import 'brace/mode/java'
import 'brace/mode/css'
import 'brace/mode/html'
import 'brace/mode/json'
import 'brace/mode/jsx'
import 'brace/mode/php'
import 'brace/mode/python'
import 'brace/mode/sql'

import 'brace/theme/monokai'
import 'brace/theme/github'
import 'brace/theme/tomorrow'
import 'brace/theme/kuroir'
import 'brace/theme/twilight'
import 'brace/theme/xcode'

class TheEditor extends React.Component {
  constructor(props) {
    super(props)
    this.changeEditor = 1
    this.socket = io('http://localhost:3001')
    this.state = {
      editorContent: ''
    }
  }

  componentWillMount() {
    this.emitConnection()
  }

  componentDidMount() {
    this.socket.on('test2', (test2) => {
      this.setState({ editorContent: test2 })
    })
  }

  onChange(text) {
    clearTimeout(this.changeEditor)
    this.changeEditor = setTimeout(() => { this.sendText(text) }, 5000)
  }

  emitConnection() {
  }

  sendText(text) {
    const { data } = this.props
    const json = {
      idCours: data,
      text
    }
    this.socket.emit('test', json)
  }

  render() {
    const { data } = this.props
    // const { editorContent } = this.state

    if (data && !Object.keys(data).length) {
      return null
    }

    return (
      <div className="editorContainer">
        <p>{`Bienvenu au cours de ${data.mode}`}</p>
        <AceEditor
          value={data.content}
          onChange={e => this.onChange(e)}
          className="editor"
          mode={data.mode}
          theme={data.theme}
          name="eclipse"
          editorProps={{ $blockScrolling: true }}
        />
      </div>
    )
  }
}

export default TheEditor
