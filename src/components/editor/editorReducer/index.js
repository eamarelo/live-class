import { fromJS } from 'immutable'

import initialState from './initial-state'
import actionsType from '../editorActions/actions-type'

const getEditors = (state, action) => (
  fromJS(state)
    .setIn(['data'], action.data)
    .toJS()
)

const details = (state = initialState, action) => {
  switch (action.type) {
    case actionsType.GET_EDITOR:
      return getEditors(state, action)
    default:
      return state
  }
}

export default details
