import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getEditorData } from './editorActions'
import TheEditor from './editorComponents/theEditor/index.js'

class Editor extends Component {
  constructor(props) {
    super(props)
    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
    const { match } = this.props
    const idUrl = match.params.id
    getEditorData(idUrl)
  }

  render() {
    const { editor } = this.props

    return (
      <div>
        <TheEditor data={editor.data} />
      </div>
    )
  }
}

export default connect(state => state)(Editor)

