import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from './components/home'
import Header from './components/header'
import Footer from './components/footer'
import Editor from './components/editor'
import Details from './components/details'
import Login from './components/user/components/Login'
import Register from './components/user/components/Register'
import Admin from './components/admin'

class Routes extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Header />
            <Switch>
              <Route path="/" component={Login} exact />
              <Route path="/home" component={Home} exact />
              <Route path="/registration" component={Register} exact />
              <Route path="/details/:id" component={Details} exact />
              <Route path="/editor/:id" component={Editor} exact />
              <Route path="/admin" component={Admin} exact />
            </Switch>
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    )
  }
}

export default Routes
