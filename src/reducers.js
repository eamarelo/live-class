import { combineReducers } from 'redux'

import home from './components/home/homeReducer'
import details from './components/details/detailsReducer'
import editor from './components/editor/editorReducer'
import error from './components/user/reducers/errorReducer'
import authReducer from './components/user/reducers/authReducer'

export default combineReducers({
  home,
  details,
  editor,
  errors: error,
  auth: authReducer
})
